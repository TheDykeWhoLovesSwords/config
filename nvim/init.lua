require('plugins')
require('env')
--Colour Scheme
require('colourscheme')
--Auto Completion & LSP
require('nv-compe')
require('lsp')
require'lspconfig'.pyright.setup{}
require'lspconfig'.bashls.setup{}
require'lspconfig'.cssls.setup{}
require 'colorizer'.setup()
require'lspconfig'.yamlls.setup{}
--Buffer Line
--require('nv-bufferline')
