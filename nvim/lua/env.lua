--Settings 
vim.wo.number=true
vim.o.termguicolors=true
vim.o.cursorline=true
vim.o.cursorcolumn=true
--Keymaps
vim.api.nvim_set_keymap('n','<Up>','<NOP>', {noremap=true,silent=true})
vim.api.nvim_set_keymap('n','<Down>','<NOP>', {noremap=true,silent=true})
vim.api.nvim_set_keymap('n','<Left>','<NOP>', {noremap=true,silent=true})
vim.api.nvim_set_keymap('n','<Right>','<NOP>', {noremap=true,silent=true})
--Leader Key
vim.g.mapleader=' '
--File Explorer
vim.api.nvim_set_keymap('n','<Leader>e',':NvimTreeToggle<CR>',{noremap=true,silent=true})
vim.api.nvim_set_keymap('n','<Leader>q',':q<CR>',{noremap=true,silent=true})
--Window Navigation
vim.api.nvim_set_keymap('n','<C-h>','<C-w>h',{silent=true})
vim.api.nvim_set_keymap('n','<C-j>','<C-w>j',{silent=true})
vim.api.nvim_set_keymap('n','<C-k>','<C-w>k',{silent=true})
vim.api.nvim_set_keymap('n','<C-l>','<C-w>l',{silent=true})
