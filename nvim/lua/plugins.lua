local execute = vim.api.nvim_command
local fn = vim.fn

local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
--[[
if fn.empty(fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
  execute 'packadd packer.nvim'
end
--]]

return require('packer').startup(function()
  -- Packer can manage itself as an optional plugin
  use {'wbthomason/packer.nvim', opt = true}
  use 'itchyny/lightline.vim'
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/nvim-compe'
  use 'anott03/nvim-lspinstall'
  use 'hrsh7th/vim-vsnip'
  use 'norcalli/nvim-colorizer.lua'
  use 'mhinz/vim-startify'
  use 'kyazdani42/nvim-tree.lua'
  use {'nvim-treesitter/nvim-treesitter',run=':TSUpdate'}
  use 'christianchiarulli/nvcode-color-schemes.vim'
  use {'akinsho/nvim-bufferline.lua', requires = 'kyazdani42/nvim-web-devicons'}

end)
